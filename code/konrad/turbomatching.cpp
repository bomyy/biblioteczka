struct matching {
	int n;
	vector <vector<int>> G;
	vector <int> vis;
	vector <int> para;
	vector <int> strona; // wszystkie wierzchołki znajdujące się po lewej stronie grafu

	matching(int x) { // podajemy wartość największego wierzchołka, numerujemy od 1
		G.resize(x + 1);
		vis.resize(x + 1);
		para.resize(x + 1);
		n = x;
	}

	void add_edge(int a, int b) { // dodany wierzchołek A jest po lewej stronie
		G[a].push_back(b);
		strona.push_back(a);
	}

	vector<pair<int, int> > go() { // zwraca vector sparować lewej strony z prawą stroną
		sort(strona.begin(), strona.end());
		strona.resize(unique(strona.begin(), strona.end()) - strona.begin());

		for(int i = 0; i < n; i++) {
			random_shuffle(G[i].begin(), G[i].end());
		}
		sort(strona.begin(), strona.end(), [&](int a, int b){return G[a].size() < G[b].size();});

		bool ok = 0;

		do {
			ok = 0;
			for(auto i : strona) vis[i] = 0;
			for(auto i : strona) {
				if(!para[i] and skojarz(i)) {
					ok = 1;
				}
			}
		} while(ok);

		vector <pair<int, int> > ret;

		for(auto i : strona) {
			if(para[i]) {
				ret.push_back({i, para[i]});
			}
		}
		return ret;
	}

	bool skojarz(int v) { // funkcja wewnętrzna matchingu, jej nie chcemy wywoływać
		vis[v] = 1;
		for(auto u : G[v]) {
			if(!para[u] or (!vis[para[u]] and skojarz(para[u]))) {
				para[u] = v;
				para[v] = u;
				return true;
			}
		}
		return false;
	}
};