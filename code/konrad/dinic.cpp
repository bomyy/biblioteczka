struct dinic {

    int n;
    int s; // źródło
    int t; // ujście
    vector <vector<int> > G;
    vector <vector<int> > cap;
    vector <int> lvl;

    dinic(int n, int s, int t) {
        this->s = s;
        this->t = t;
        this->n = n;
        G.resize(n + 1);
        lvl.resize(n + 1);
        cap.resize(n + 1, vector<int>(n + 1, 0));
    }

    void add_edge(int a, int b, int c) {
        if(cap[a][b] == 0) G[a].push_back(b);
        cap[a][b] += c;
    }

    bool bfs() {
        queue <int> q;
        q.push(s);
        for(int i = 0; i <= n; i++) {
            lvl[i] = -1;
        }
        lvl[s] = 0;

        while(!q.empty()) {
            auto v = q.front();
            q.pop();

            for(auto it : G[v]) {
                if(lvl[it] == -1 and cap[v][it] > 0) {
                    lvl[it] = lvl[v] + 1;
                    q.push(it);
                }
            }
        }

        return lvl[t] != -1;
    }

    int dfs(int v, int val) {
        if(v == t or val == 0) {
            return val;
        }

        int ret = 0;

        for(auto it : G[v]) {
            if(lvl[it] == lvl[v] + 1 and cap[v][it] > 0) {
                int tmp = dfs(it, min(val, cap[v][it]));

                ret += tmp;
                val -= tmp;

                cap[v][it] -= tmp;
                cap[it][v] += tmp;

                if(val == 0) break;
            }
        }

        return ret;
    }

    long long flow() {
        long long max_flow = 0;

        while(bfs()) {
            max_flow += dfs(s, INF);
        }

        return max_flow;
    }
};